@extends('layouts.app')

@section('content')

@php 
    $type['VD'] = 'Venta Débito';
    $type['VN'] = 'Venta Normal';
    $type['VC'] = 'Venta en cuotas';
    $type['SI'] = '3 cuotas sin interés';
    $type['S2'] = '2 cuotas sin interés';
    $type['NC'] = 'N Cuotas sin interés';

    @$pay = json_decode($rej);

@endphp
    <div class="ct-info-cart">
        <section>
            <div class="ct-info col-12">
                <div class="row">
                    <div class="col-12 ct-text-op">
                    <div class="row line-cart">
                        <div class="col-12 col-md-4 box">
                            <div class="row justify-content-md-start justify-content-center">
                                <span class="label_detail">Detalle</span>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 box">
                            <div class="row justify-content-center">
                                <span class="label_payment">Pago</span>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 box">
                            <div class="row justify-content-md-end justify-content-center">
                                <span class="label_voucher active">Comprobante</span>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    <div class="col-12 ct-voucher">
                        <h2>Gracias por comprar en {{@$data->name}}</h2>

                        <div class="row justify-content-center pt-5">
                            <div class="col-md-6 text-md-right">Número de orden de Pedido </div>
                            <div class="col-md-6"><b>{{@$pay->result->buyOrder}}</b></div>

                            <!--div class="col-md-6 text-md-right">Nombre del comercio</div>
                            <div class="col-md-6"><b>{{@$data->name}}</b></div-->
                            
                            <div class="col-md-6 text-md-right">Monto y moneda de la transacción</div>
                            <div class="col-md-6"><b>{{@$pay->result->detailOutput->amount}}</b></div>
                            
                            <div class="col-md-6 text-md-right">Código de autorización de la transacción</div>
                            <div class="col-md-6"><b>{{@$pay->result->detailOutput->authorizationCode}}</b></div>
                            
                            <div class="col-md-6 text-md-right">Fecha de la transacción</div>
                            <div class="col-md-6"><b>{{@$pay->result->transactionDate}}</b></div>
                            
                            <div class="col-md-6 text-md-right">Tipo de pago realizado</div>
                            <div class="col-md-6"><b>{{$type[@$pay->result->detailOutput->paymentTypeCode]}}</b></div>
                            
                            <div class="col-md-6 text-md-right">Cantidad de cuotas</div>
                            <div class="col-md-6"><b>{{@$pay->result->detailOutput->sharesNumber}}</b></div>
                            
                            <div class="col-md-6 text-md-right">4 últimos dígitos de la tarjeta bancaria</div>
                            <div class="col-md-6"><b>{{@$pay->result->cardDetail->cardNumber}}</b></div>
                            
                            <div class="col-md-6 text-md-right">Descripción de los bienes y/o servicios</div>
                            <div class="col-md-6"><b>{{@$data->description}}</b></div>
                            
                        </div>

                        <p>Hemos enviado tu comprobante de compra y tu confirmación <br class="d-none d-lg-block">al mail <b> {{@$data->email}}</b></p>
                        <div class="ct-buttons d-flex flex-md-row flex-column justify-content-center align-items-center">
                            <a href="{{@$data->callback}}" class="return">Volver al Home</a>
                        </div>
                    </div>
                </div>
            </div>
        </section> 
    </div>

@endsection


@section('scripts')
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>
@endsection 