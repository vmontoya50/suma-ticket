@extends('layouts.app')

@section('content')

    <div class="ct-info-cart">
        <section>
            <div class="ct-info col-12">
                <div class="row">
                    
                    <div class="col-12 ct-voucher">
                        <h2 class="text-red">La orden de compra fue rechazada </h2>
                        <p>Las posibles causas de este rechazo son: <br class="d-none d-lg-block"></p>
                        <ul>
                            <li> Error en el ingreso de los datos de su tarjeta de Crédito o Débito (fecha y/o código de seguridad).</li>
                            <li> Su tarjeta de Crédito o Débito no cuenta con saldo suficiente.</li>
                            <li> Tarjeta aún no habilitada en el sistema financiero.</li>
                        </ul>
                        <div class="ct-buttons d-flex flex-md-row flex-column justify-content-center align-items-center">
                            <a href="/event/1" class="btn btn-primary">Volver al Home</a>
                        </div>
                    </div>
                </div>
            </div>
        </section> 
    </div>

@endsection


@section('scripts')
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>
@endsection 