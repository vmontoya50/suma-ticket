<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('ticket_event', 'TicketEventController');
Route::resource('ticket', 'TicketController');
Route::resource('country', 'CountryController');
Route::resource('buyer', 'BuyerController');
Route::resource('event', 'EventController');
Route::resource('event.audience', 'EventAudienceController');
Route::resource('calendarevent', 'CalendarEventController');
Route::resource('payment', 'PaymentController');
Route::resource('segment', 'SegmentController');
Route::resource('location', 'LocationController');