<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/success', 'PaymentController@webpay')->name('/');
Route::get('/event/{event_id}', function ($event_id) {
    return view('events.index',['id' => $event_id,'ticket_event'=> null]);
});
Route::get('/event/{event_id}/ticket/{ticket_event?}', function ($event_id, $ticket_event = null) {
    return view('events.index',['id' => $event_id,'ticket_event'=> $ticket_event]);
});
Route::get('/success', function () {
    return view('events.success');
});
// Email related routes
Route::get('mail/send', 'MailController@send');