<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_events', function (Blueprint $table) {
            $table->increments('id');        
            $table->integer('id_calendar');        
            $table->integer('id_segment');        
            $table->integer('ticket_limit');
            $table->timestamp('start_date');
            $table->date('expiration_date');
            $table->string('description');        
            $table->integer('id_audience');      
            $table->integer('price');
            $table->timestamps();          
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_events');
    }
}
