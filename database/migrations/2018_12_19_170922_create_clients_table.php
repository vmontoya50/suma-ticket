<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('url');
            $table->boolean('webpay_enabled');
            $table->boolean('paypal_enabled');
            $table->text('webpay_cert');
            $table->text('commerce_code');
            $table->text('private_key');
            $table->text('public_cert');
            $table->timestamps();           
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
