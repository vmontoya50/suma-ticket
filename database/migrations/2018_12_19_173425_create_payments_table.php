<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {

            $table->increments('id'); 
            $table->integer('id_buyer');
            $table->string('buy_order',26);
            $table->string('session_id',61);
            $table->string('card_number',16);
            $table->string('card_expiration_date',4);
            $table->string('accouting_date',4);
            $table->string('transaction_date',40); 
            $table->string('vci',6);            
            $table->string('authorization_code',20);            
            $table->string('payment_type_code',20);                         
            $table->string('response_code',20);                        
            $table->string('amount',10);
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
