<?php

use Illuminate\Database\Seeder;

class CalendarEventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('calendar_events')->insert([
            'id_event' => 1,
            'day' => date("2019-06-06"),
        ]);

        DB::table('calendar_events')->insert([
            'id_event' => 1,
            'day' => date("2019-06-07"),
        ]);
        DB::table('calendar_events')->insert([
            'id_event' => 1,
            'day' => date("2019-06-08"),
        ]);
        

       /* $calendar = factory(App\CalendarEvent::class, 100)->create();

        $calendar->each(function($c){
            factory(App\TicketEvent::class, 2)->create([
                'id_calendar' => $c->id,
                'start_date' => $c->day,
                'expiration_date' => $c->day,
            ]);
        });*/
    }
}
