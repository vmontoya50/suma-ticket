<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('events')->insert([
            'name' => 'Congreso CILME',
            'color1' => '#c6a366',
            'color2'  => '#8d8d8d',
            'callback'  => 'https://cilme2019.com/',
            'id_client'  => 1,
        ]);
    }
}
