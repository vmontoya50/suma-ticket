<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SegmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //segments
        DB::table('segments')->insert([
            'id_location' => 1,
            'name' => 'General',
        ]);
        DB::table('segments')->insert([
            'id_location' => 1,
            'name' => 'Escenario',
        ]);
    }
}
