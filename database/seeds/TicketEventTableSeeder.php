<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketEventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('ticket_events')->insert([
            'id_calendar' => 1,
            'id_segment' =>1,
            'ticket_limit' => 400,
            'start_date' => '2019-01-01',
            'expiration_date' => '2019-06-06',
            'description' => 'Congreso',
            'id_audience' => 1,
            'price' => 80000,
        ]);

      /*  DB::table('ticket_events')->insert([
            'id_calendar' => 2,
            'id_segment' =>1,
            'ticket_limit' => 15,
            'start_date' => '2019-01-01',
            'expiration_date' => '2019-06-07',
            'description' => 'Congreso ',
            'id_audience' => 1,
            'price' => 80000,
        ]);

        DB::table('ticket_events')->insert([
            'id_calendar' => 3,
            'id_segment' =>1,
            'ticket_limit' => 15,
            'start_date' => '2019-01-01',
            'expiration_date' => '2019-06-08',
            'description' => 'Congreso',
            'id_audience' => 1,
            'price' => 80000,
        ]);*/
    }
}
