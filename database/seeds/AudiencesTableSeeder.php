<?php

use Illuminate\Database\Seeder;

class AudiencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //audiences
        DB::table('audiences')->insert([
            'name_audience' => 'General',
            'id_event' => 1,
        ]);
        DB::table('audiences')->insert([
            'name_audience' => 'Orador',
            'id_event' => 1,
        ]);
    }
}
