<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //countries
        DB::table('countries')->insert([
            'name' => 'Chile',
        ]);
    }
    
}
