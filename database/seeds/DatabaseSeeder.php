<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(AudiencesTableSeeder::class);
        $this->call(CalendarEventTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(RegionTableSeeder::class);
        $this->call(SegmentsTableSeeder::class);
        $this->call(TicketEventTableSeeder::class);

    }
}
