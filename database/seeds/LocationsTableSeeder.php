<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //locations 
        DB::table('locations')->insert([
            'id_country' => 1,
            'id_region'  => 1,
            'name' => 'CILME',
        ]);

        DB::table('locations')->insert([
            'id_country' => 1,
            'id_region'  => 1,
            'name' => 'Movistar Arena',
        ]);
    }
}
