<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\CalendarEvent::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'id_event' => 1,
        'day' => '2019-'.rand(1,6).'-'.rand(5,30),
    ];
});

$factory->define(App\TicketEvent::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'id_calendar' => 1,
        'id_segment' =>1,
        'ticket_limit' => 15,
        'start_date' => '2019-01-30',
        'expiration_date' => '2019-01-30',
        'description' => $faker->realText($faker->numberBetween(10,20)),
        'id_audience' => 1,
        'price' => 20000,
    ];
});
