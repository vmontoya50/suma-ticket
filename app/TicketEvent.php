<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Ticket;
use App\Event;

class TicketEvent extends Model
{
    //
    //protected $fillable = ['ticket_limit'];
    public static function EventDate($id){

        $ticket_count = Ticket::all()
        ->where('status',1)
        ->where('id_ticket_event',$id)->count();




        $nuevafecha = strtotime ( '+5 minute' , strtotime ( date('Y-m-d H:i:s') ) ) ;
        $expiration_date = date ( 'Y-m-d H:i:s', $nuevafecha );

        $ticket_reservation = DB::table('tickets')
                    ->where('status',0)
                    ->whereBetween('expiration_date', [date('Y-m-d H:i:s'), $expiration_date])
                    ->where('id_ticket_event',$id)->count();



        $event = TicketEvent::where('ticket_events.id',$id)
        ->join('calendar_events', 'ticket_events.id_calendar', '=', 'calendar_events.id')        
        ->join('events', 'calendar_events.id_event', '=', 'events.id')
        ->join('audiences', 'ticket_events.id_audience', '=', 'audiences.id')
        ->get();

        $event = json_decode($event);
        $event = $event[0];

        $var['events']['name']               = $event->name;
        $var['calendar_events']['day']       = $event->day;
        $var['ticket_events']['description'] = $event->description;
        $var['ticket_events']['limit']       = $event->ticket_limit;        
        $var['ticket_events']['price']       = $event->price;
        $var['ticket_events']['purchased']   = $ticket_count;        
        $var['ticket_events']['reserved']    = $ticket_reservation;
        $var['ticket_events']['avaible']     = ($event->ticket_limit - ($ticket_count+$ticket_reservation));

        return $var;
    }

    public static function EventDateDetail($id){

        $ticket_count = Ticket::all()
        ->where('status',1)
        ->where('id_ticket_event',$id)->count();




        $nuevafecha = strtotime ( '+5 minute' , strtotime ( date('Y-m-d H:i:s') ) ) ;
        $expiration_date = date ( 'Y-m-d H:i:s', $nuevafecha );

        $ticket_reservation = DB::table('tickets')
                    ->where('status',0)
                    ->whereBetween('expiration_date', [date('Y-m-d H:i:s'), $expiration_date])
                    ->where('id_ticket_event',$id)->count();



        $event = TicketEvent::where('ticket_events.id',$id)
        ->join('calendar_events', 'ticket_events.id_calendar', '=', 'calendar_events.id')        
        ->join('events', 'calendar_events.id_event', '=', 'events.id')
        ->join('audiences', 'ticket_events.id_audience', '=', 'audiences.id')
        ->get();

        $event = json_decode($event);
        $event = $event[0];

        //$var['events']['name']               = $event->name;
        //$var['calendar_events']['day']       = $event->day;
        $var['description'] = $event->description;
        $var['audience']    = $event->name_audience;
        $var['limit']       = $event->ticket_limit;        
        $var['price']       = $event->price;
        $var['purchased']   = $ticket_count;        
        $var['reserved']    = $ticket_reservation;
        $var['avaible']     = ($event->ticket_limit - ($ticket_count+$ticket_reservation));

        return $var;
    }  
}
