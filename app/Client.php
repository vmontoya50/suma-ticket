<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    public static function SetClient($var){

        if(!empty($var->name)&&!empty($var->url)&&!empty($var->webpay_enabled)&&!empty($var->paypal_enabled)){

            $client = new Client;
            $client->name           = $var->name;
            $client->url            = $var->url;
            $client->webpay_enabled = $var->webpay_enabled;
            $client->paypal_enabled = $var->paypal_enabled;   
            $client->save();
            
            $re['result']['idClient']  = $client->id;
            $re['message']['details']  = 'Client created';
            $re['status']  = 201;

        }else{          
            
            $re['status']  = 400;
            $re['message']['details'] = "Bad Request";
            
        }

    }
}
