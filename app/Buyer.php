<?php

namespace App;

use App\Payment;
use App\Ticket;
use App\TicketEvent;
use App\TicketPrice;
use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    //
    public static function SetBuyer($var)
    {
        if (empty($var->name)
            || empty($var->lastname)
            || empty($var->type_identification)
            || empty($var->identification)
            || empty($var->email)
            || empty($var->phone)
            || empty($var->id_ticket_event)
            || empty($var->amount)) {
            $re['status'] = 405;
            $re['message']['details'] = "Method Not Allowed, empty fields";

            return $re;
            exit();
        }

        $amount = (int) $var->amount;
        $ticketEvent = TicketEvent::EventDate($var->id_ticket_event);

        if ($amount < 11 && $ticketEvent['ticket_events']['avaible'] > $amount) {

            $buyer = new Buyer;
            $buyer->name = $var->name;
            $buyer->lastname = $var->lastname;
            $buyer->type_identification = $var->type_identification;
            $buyer->identification = $var->identification;
            $buyer->email = $var->email;
            $buyer->phone = $var->phone;
            $buyer->save();

            //echo "price ".$ticketEvent['ticket_events']['price'] * $amount;

            $newPrice = TicketPrice::GetTicketPrice($var->id_ticket_event);

            if(@$newPrice){
                //print_r($newPrice);
                $ticketEvent['ticket_events']['price'] = $newPrice->price_temp;
            }

            @$pay->price = $ticketEvent['ticket_events']['price'] * $amount;
            @$pay->buyOrder = $buyer->id;            
            @$pay->id_event = $var->id_ticket_event;
            @$pay->email = $var->email;



            $ticket = Ticket::SetTickets($var, $buyer->id);
            $webpay = Payment::InitWebPay($pay);
            //$tbkToken   = $webpay["initResult"]->token;

            $re['webpay'] = $webpay;
            $re['result']['IdBuyer'] = $buyer->id;
            $re['message']['details'] = 'Buyer created';
            $re['status'] = 201;

        } else {

            if ($ticketEvent['ticket_events']['avaible'] < $amount) {
                $re['status'] = 409;
                $re['message']['details'] = "Ticket exhausted";
            } else {
                $re['status'] = 400;
                $re['message']['details'] = "Bad Request";
            }

        }

        return $re;

    }

    public static function GetBuyerEvent($id){
        $data = Buyer::where('buyers.id',$id)
                //->select('clients.name as name_client', 'ticket_events')
                ->join('tickets','buyers.id','=','tickets.id_buyer')
                ->join('ticket_events', 'tickets.id_ticket_event','=','ticket_events.id') 
                ->join('calendar_events', 'ticket_events.id_calendar','=','calendar_events.id')  
                ->join('events', 'calendar_events.id_event','=','events.id') 
                ->join('clients', 'events.id_client','=','clients.id')                
                ->first();

        return $data;
    }
}
