<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Buyer;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //print_r($request);
        //Payment::TransactionResult($request);
        return view('welcome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $payment = Payment::SetPayment($request);
        return $payment;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        Payment::TransactionResult($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function webpay(Request $request){

        if($request->input("token_ws")){

            $re = Payment::TransactionResult($request->input("token_ws"));

            if(@$re['payment']['idPayment']){

                //$data = Buyer::where('id',$re['payment']['idPayment'])->first();
                $data = Buyer::GetBuyerEvent($re['payment']['idPayment']);

            }else{
                $data = null;
            }            
        }

        $rej = json_encode($re);
        if(@$re['payment']['status'] == '201' ){            
            return view('events.success', compact('data', 'rej'));
        }
    
        if(@(int)@$re['status'] > 201 ){            
            return view('events.error', compact('data', 'rej'));
        }
        if(@(int)@$re['status'] == '' ){            
            return view('events.error', compact('data', 'rej'));
        }
    }
}
