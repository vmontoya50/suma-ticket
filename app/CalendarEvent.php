<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarEvent extends Model
{
    //
    public static function SetCalendar($var){

        if(!empty($var->id_event)&&!empty($var->day)){

            $calendar = new CalendarEvent;
            $calendar->id_event = $var->id_event;
            $calendar->day      = $var->day;
            $calendar->save();
            
            $re['result']['idCalendarEvent']  = $calendar->id;
            $re['message']['details']         = 'Calendar event created';
            $re['status']  = 201;

        }else{          
            
            $re['status']  = 405;
            $re['message']['details'] = "Method Not Allowed, empty fields";
            
        }
        return $re;

    }
}
