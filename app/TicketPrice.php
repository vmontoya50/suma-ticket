<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TicketPrice extends Model
{
    //a
    public static function GetTicketPrice($id){
       // $ticket = TicketPrice::where('id',$id)->get();

       //$dateInit = date('Y-m-d H:i:s');
       //$dateFinish = (date("Y")+1).date('-m-d H:i:s');

       $dateInit = date('Y-m-d H:i:s');//'2019-04-16';
       $dateFinish = (date('Y')+1).date('-m-d H:i:s'); //'2020-04-16';
       $dateInitB = (date('Y')-1).date('-m-d H:i:s');//'2018-04-16'.date(' H:i:s');
       $dateFinishB = date('Y-m-d H:i:s');// '2019-04-16'.date(' H:i:s');

        $ticket = DB::table('ticket_prices')
                ->where('id_ticket_event','=',  $id)
                ->whereBetween('price_expiration_date', [$dateInit,$dateFinish])
                ->whereBetween('price_start_date', [$dateInitB, $dateFinishB])
                ->get();

        $ticket = json_decode($ticket);

        if(empty($ticket[0])){
            $ticket[0] = null;
        }

        return $ticket[0];
    }
}
