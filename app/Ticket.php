<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    public static function SetTickets($var, $id){

        $amount = (int)$var->amount;

        $nuevafecha = strtotime ( '+5 minute' ,   strtotime ( date('Y-m-d H:i:s') ) ) ;
        $expiration_date = date ( 'Y-m-d H:i:s' , $nuevafecha );

        if(is_numeric($amount) && $amount<11 ){
            for($i=0;$i<$amount;$i++){
                

                 $ticket = new Ticket;
                 $ticket->id_ticket_event   = $var->id_ticket_event;
                 $ticket->id_buyer          = $id; 
                 $ticket->status            = 0;
                 $ticket->expiration_date   = $expiration_date;
                 $ticket->save();

                 if($i<$amount&&isset($ticket->id)){
                    $re['message'] = '';
                    $re['status']  = 201;
                 }
     
             }  

        }else{
            $re['status']  = 400;
            $re['message'] = "Bad Request";
        }

        return $re;

    }

    public static function PayTicket($var){

        Ticket::where('id_buyer',$var)
        ->update(['status' => 1]);

    }
}
