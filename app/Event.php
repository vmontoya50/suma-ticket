<?php

namespace App;

use App\CalendarEvent;
use App\TicketEvent;
use App\TicketPrice;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //

    public static function SetEvent($var){

        if(!empty($var->name)&&!empty($var->color1)&&!empty($var->color2)&&!empty($var->callback)&&!empty($var->id_client)){

            $event = new Event;
            $event->name      = $var->name;
            $event->color1    = $var->color1;
            $event->color2    = $var->color2;
            $event->callback  = $var->callback;        
            $event->id_client = $var->id_client; 
            $event->save();
            
            $re['result']['idEvent']  = $event->id;
            $re['message']['details'] = 'Event created';
            $re['status']  = 201;

        }else{          
            
            $re['status']  = 400;
            $re['message']['details'] = "Bad Request";
            
        }
        return $re;

    }

    public static function GetEvents(){
        return Event::all();
    }

    public static function GetEvent($id){

        $event = Event::where('events.id',$id)
        //->join('calendar_events', 'events.id', '=', 'calendar_events.id_event')       
        ->get();

        $event = json_decode($event);
        $event = $event[0];

        if(!empty($event)){           

            $var['event']['name'] = $event->name;
            $var['event']['color1'] = $event->color1;
            $var['event']['color2'] = $event->color2;
            $var['event']['color1'] = $event->callback;
            $var['event']['color2'] = $event->id_client;

            $calendar = CalendarEvent::where('id_event',$id)
            //->join('calendar_events', 'events.id', '=', 'calendar_events.id_event')       
            ->get();

            $calendar = json_decode($calendar);
            
            for($i=0;$i<count($calendar);$i++){

                $idb = (int)$calendar[$i]->id;

                $ti = DB::table('ticket_events')
                ->select('*', 'ticket_prices.id as idp', 'ticket_events.id as idt')
                ->where('ticket_events.id_calendar','=',  $idb)
                ->whereBetween('expiration_date', [date('Y-m-d H:i:s'), (date("Y")+1).date('-m-d H:i:s')])
                ->leftJoin('ticket_prices', 'ticket_events.id', '=', 'ticket_prices.id_ticket_event')              
                //->orderBy('ticket_events.start_date')
                ->groupBy('ticket_events.id')
                ->get();

                $ti  = json_decode($ti);
                
                for($e=0;$e<count($ti);$e++){

                        $tickeGenerate['id'] = $ti[$e]->idt;
                        $tickeGenerate['description'] = $ti[$e]->description;                        
                        $tickeGenerate['price'] = $ti[$e]->price;

                        if(!empty($ti[$e]->idp)){
                            $tickeGenerate['idp'] = $ti[$e]->idp;
                            $ticketPrice = TicketPrice::GetTicketPrice($ti[$e]->idt);
                            //$tickeGenerate['price_'] = $ticketPrice ;
                            //print_r($ticketPrice);
                            if( $ticketPrice != null){
                                $tickeGenerate['price'] = $ticketPrice->price_temp;
                            }

                        }else{
                            $tickeGenerate['idp'] = '';
                        }
                        $var['calendar'][$calendar[$i]->day][$e] =  $tickeGenerate;

                }

            }

            
        }else{
            $var['status']  = 400;
            $var['message'] = "Bad Request"; 
        }
        return $var;

        


    }

    public static function GetEventAudience($re){

        $id = $re->id;
        $audience = $re->audience;
      
        $event = Event::where('events.id',$id)
        //->join('calendar_events', 'events.id', '=', 'calendar_events.id_event')       
        ->get();
        
        $event = json_decode($event);
        $event = $event[0];

        if(!empty($event)){           

            $var['event']['name'] = $event->name;
            $var['event']['color1'] = $event->color1;
            $var['event']['color2'] = $event->color2;
            $var['event']['color1'] = $event->callback;
            $var['event']['color2'] = $event->id_client;

            $calendar = CalendarEvent::where('id_event',$id)
            //->join('calendar_events', 'events.id', '=', 'calendar_events.id_event')       
            ->get();

            $calendar = json_decode($calendar);
            
            for($i=0;$i<count($calendar);$i++){

                $idb = (int)$calendar[$i]->id;

                $ti = DB::table('ticket_events')
                ->select('*', 'ticket_prices.id as idp', 'ticket_events.id as idt')
                ->where('ticket_events.id_calendar','=',  $idb)
                ->where('ticket_events.id_audience','=',  $audience)
                ->whereBetween('expiration_date', [date('Y-m-d H:i:s'), (date("Y")+1).date('-m-d H:i:s')])
                ->leftJoin('ticket_prices', 'ticket_events.id', '=', 'ticket_prices.id_ticket_event')              
                //->orderBy('ticket_events.start_date')
                ->groupBy('ticket_events.id')
                ->get();

                $ti  = json_decode($ti);
                
                for($e=0;$e<count($ti);$e++){

                        $tickeGenerate['id'] = $ti[$e]->idt;
                        $tickeGenerate['description'] = $ti[$e]->description;                        
                        $tickeGenerate['price'] = $ti[$e]->price;

                        if(!empty($ti[$e]->idp)){
                            $tickeGenerate['idp'] = $ti[$e]->idp;
                            $ticketPrice = TicketPrice::GetTicketPrice($ti[$e]->idt);
                            //$tickeGenerate['price_'] = $ticketPrice ;
                            //print_r($ticketPrice);
                            if( $ticketPrice != null){
                                $tickeGenerate['price'] = $ticketPrice->price_temp;
                            }

                        }else{
                            $tickeGenerate['idp'] = '';
                        }
                        $var['calendar'][$calendar[$i]->day][$e] =  $tickeGenerate;

                }

            }

            
        }else{
            $var['status']  = 400;
            $var['message'] = "Bad Request"; 
        }
        return $var;

        


    }
}
