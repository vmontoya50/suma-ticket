<?php

namespace App;

use Transbank\Webpay\Configuration;
use Transbank\Webpay\Webpay;
use Illuminate\Database\Eloquent\Model;
use App\Buyer;
use App\Ticket;

class Payment extends Model
{
    //
    public static function SetPayment($var){

        if(!empty($var)
          &&!empty($var->buyOrder)
          &&!empty($var->sessionId)
          &&!empty($var->cardDetail->cardNumber)
          &&!empty($var->VCI)
        ){
            $paymen = new Payment;

           if(empty($var->cardDetail->cardExpirationDate)){
            $var->cardDetail->cardExpirationDate = 0;
           }

            $paymen->id_buyer              = $var->buyOrder;
            $paymen->buy_order             = $var->buyOrder;
            $paymen->session_id            = $var->sessionId;
            $paymen->card_number           = $var->cardDetail->cardNumber;
            $paymen->card_expiration_date  = $var->cardDetail->cardExpirationDate;
            $paymen->accouting_date        = $var->accountingDate;
            $paymen->transaction_date      = $var->transactionDate;        
            $paymen->vci                   = $var->VCI;
            $paymen->authorization_code    = $var->detailOutput->authorizationCode;
            $paymen->payment_type_code     = $var->detailOutput->paymentTypeCode;          
            $paymen->amount                = $var->detailOutput->amount;
            $paymen->response_code         = $var->detailOutput->responseCode;

            $paymen->save();

            if($var->detailOutput->responseCode == 0){
                Ticket::PayTicket($var->buyOrder);
                
                $re['idPayment']  = $var->buyOrder;
                $re['message']['details']   = 'Payment created';
                $re['status']  = 201;
            }else{
                $re['idPayment']  = $var->buyOrder;
                $re['message']['details']   = self::PayStatus($var->detailOutput->responseCode);
                $re['status']  = 406;
            }           

        }else{          
            
            $re['status']  = 400;
            $re['message']['details'] = "Bad Request";
            
        }

        return $re;
        
    }

    public static function PayStatus($id){

        $detail['-1'] = 'Rechazo de transacción.';
        $detail['-2'] = 'Transacción debe reintentarse.';
        $detail['-3'] = 'Error en transacción.';
        $detail['-4'] = 'Rechazo de transacción.';
        $detail['-5'] = 'Rechazo por error de tasa.';
        $detail['-6'] = 'Excede cupo máximo mensual.';
        $detail['-7'] = 'Excede límite diario por transacción.';
        $detail['-8'] = 'Rubro no autorizado.';

        return $detail[(string)$id];
    }

    public static function InitWebPay($in){

        $event = TicketEvent::where('ticket_events.id',$in->id_event)
            ->join('calendar_events', 'ticket_events.id_calendar', '=', 'calendar_events.id')        
            ->join('events', 'calendar_events.id_event', '=', 'events.id')
            ->join('clients', 'events.id', '=', 'clients.id')
            ->get();

            $event = json_decode($event);
            $event = $event[0];

            /*echo "Commerce ".$event->commerce_code." ";
            echo "private_key ".$event->private_key." ";
            echo "public_cert ".$event->public_cert." ";*/

        $configuration = new Configuration();
        $configuration->setCommerceCode((string)$event->commerce_code);
        $configuration->setPrivateKey((string)$event->private_key);
        $configuration->setPublicCert((string)$event->public_cert);
        $configuration->setWebpayCert(Webpay::defaultCert());

        //print_r($configuration);
       // exit();

        

        $transaction =
            (new Webpay($configuration))
            ->getNormalTransaction();

            

            if (isset($_SERVER['HTTPS'])) {
                // Codigo a ejecutar si se navega bajo entorno seguro.
                $ht = "https://";
            } else {
                // Codigo a ejecutar si NO se navega bajo entorno seguro.
                $ht = "";
            }

            if($_SERVER['SERVER_PORT'] == 80){
                $port = '';
            }else{
                $port = ':'.$_SERVER['SERVER_PORT'];
            }

            // Identificador del usuario en el comercio
            $username = $event->name;
            // Correo electrónico del usuario
            $email = $in->email;
          // @$returnUrl =  $ht.$_SERVER['SERVER_NAME'].$port."/success"; 
           @$returnUrl = "http://localhost:8000/success"; 
            $finalUrl = $event->callback;
            $amount = $in->price;
            $sessionId = $in->buyOrder;
            $buyOrder = $in->buyOrder;
            
            $initResult = $transaction->initTransaction($amount, $sessionId, $buyOrder, $returnUrl, $finalUrl);
            //print_r($initResult);

            $formAction = $initResult->url;
            $tbkToken   = $initResult->token;

            //$var['transaction'] = $transaction;
            $var['initResult']  = $initResult;
            $var['total']  = $in->price;
            $var['event'] = $event;
            $var['http'] = $returnUrl;

            //$initResult['total'] =  $in->price;

            return $var;

    }

    public static function TransactionResult($id){
        $transaction =
            (new Webpay(Configuration::forTestingWebpayPlusNormal()))
            ->getNormalTransaction();

            $result = $transaction->getTransactionResult($id);

            if(@$result->detailOutput->responseCode == 0){
                $re['payment'] = self::SetPayment($result);
                $re['result']  = $result;
                $re['status']  = 201;
            }else{
                $re['status']  = 400;
                $re['message'] = self::PayStatus($result->detailOutput->responseCode);
                //a
            }
            
            
            return $re;
    }

}
